import React from "react";
import ReactDOM from "react-dom";
import {
    Navbar,
    Nav,
    NavItem,
    Grid,
    Row,
    Col,
    FormGroup,
    InputGroup,
    FormControl,
    Button,
    Table
} from "react-bootstrap";
import Select from "react-select";

import { WORKERS } from "./workers.js";
import ModalComponent from "./ModalComponent.js";
import EditModalComponent from "./EditModalComponent.js";
import Quest from "./Quest.js"
import "./styles.css";

class Workers extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            sex: '',
            department: '',
            trainee: '',
            searchInput: null,
            result: WORKERS
        };

        this.handleSexChange = this.handleSexChange.bind(this);
        this.handleTraineeChange = this.handleTraineeChange.bind(this);
        this.handleDepartmentChange = this.handleDepartmentChange.bind(this);
        this.handleSearchInputChange = this.handleSearchInputChange.bind(this);
        this.handleSearchButtonClick = this.handleSearchButtonClick.bind(this);

        this.addNewWorker = this.addNewWorker.bind(this);
        this.EditWorker = this.EditWorker.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
    }

    handleSexChange(selectedOption) {
        let resultArray = WORKERS.filter(
            item => item.sex.indexOf(selectedOption.value) !== -1
            // item => item.sex == this.state.sex
        );
        resultArray = searchDepartment(resultArray, this.state.department);
        resultArray = searchTrainee(resultArray, this.state.trainee);

        this.setState({
            sex: selectedOption.value,
        });
    }



    handleTraineeChange(selectedOption) {
        let resultArray = WORKERS.filter(
            // item => item.trainee.indexOf(selectedOption.value) !== -1
            item => item.trainee == this.state.trainee
        );

        resultArray = searchSex(resultArray, this.state.sex);
        resultArray = searchDepartment(resultArray, this.state.department);

        this.setState({
            trainee: selectedOption.value,
        });
    }

    handleDepartmentChange(selectedOption) {
        let resultArray = WORKERS.filter(
            item => item.department.indexOf(selectedOption.value) !== -1
            // item => item.department == this.state.department
        );

        resultArray = searchTrainee(resultArray, this.state.trainee);
        resultArray = searchSex(resultArray, this.state.sex);

        this.setState({
            department: selectedOption.value,
        });
    }

    handleSearchInputChange(event) {
        this.setState({
            searchInput: event.target.value
        });
    }


    handleSearchButtonClick() {
        const { searchInput } = this.state;


        const resultArray = searchInput
            ? this.state.result.filter(
                item =>
                    item.last_name.toLowerCase().indexOf(searchInput.toLowerCase()) !== -1
            )
            : WORKERS;

        this.setState({
            result: resultArray
        });
    };

    
    addNewWorker(newWorker){
        this.setState({
            result: [...this.state.result, newWorker]
        })
    }

    EditWorker(index, Worker){
       
            let temp = this.state.result;
            temp[index] = Worker
          
            this.setState({result: temp})
    }

    deleteItem(index) {
        let Temp = this.state.result;
        Temp.splice(index, 1);
        this.setState({ result: Temp });
      }
 

    render() {
            
        let resultArray = this.state.sex || this.state.department || this.state.trainee ?
        this.state.result.filter(
            (item) => isSex(this.state.sex, item) && isDepartment(this.state.department, item) && isTrainee(this.state.trainee, item)
        ) : this.state.result; 
                
    function isSex(stateSex, item){
        if(stateSex == ''){
            return true
        }
        else if( stateSex == item.sex){
            return true
        }
        else{
            return false
        }
    }
    
    function isDepartment(stateDepartment, item){
        if(stateDepartment == ''){
            return true
        }
        else if( stateDepartment == item.department){
            return true
        }
        else{
            return false
        }
    }
    
    function isTrainee(stateTrainee, item){
        if(stateTrainee == ''){
            return true
        }
        else if( stateTrainee == item.trainee){
            return true
        }
        else{
            return false
        }
    }
        return (
            <div>

                <Navbar inverse>
                    <Nav>
                        
                        <NavItem eventKey={1} href="#" className="logo1">
                            <Quest/>
                        </NavItem>
                        <NavItem eventKey={2} href="https://rusbitech.ru/" className="logo1">
                            РусБИТех
             
                        </NavItem>
                    </Nav>
                    <Nav pullRight>
                        <NavItem eventKey={1} href="#" className="logo">
                            Nikita Vinogradov
            
                        </NavItem>
                    </Nav>
                </Navbar>

                <Grid>
                    <Row className="show-grid">
                        <Col xs={3}>
                            <Select
                                name="sex"
                                value={this.state.sex}
                                onChange={this.handleSexChange}
                                clearable={false}
                                options={[
                                    { value: '', label: "Пол" },
                                    { value: "мужчина", label: "Мужчина" },
                                    { value: "женщина", label: "Женщина" }
                                ]}
                            />
                        </Col>
                        <Col xs={3}>
                            <Select
                                name="trainee"
                                value={this.state.trainee}
                                onChange={this.handleTraineeChange}
                                clearable={false}
                                options={[
                                    { value: '', label: "Статус стажировки" },
                                    { value: true, label: "Стажёр" },
                                    { value: false, label: "Не стажёр" }

                                ]}
                            />
                        </Col>
                        <Col xs={3}>
                            <Select
                                name="department"
                                value={this.state.department}
                                onChange={this.handleDepartmentChange}
                                clearable={false}
                                options={[
                                    { value: '', label: "Отдел" },
                                    { value: "Testing", label: "Testing" },
                                    { value: "Back-end", label: "Back-end" },
                                    { value: "Analytics", label: "Analytics" },
                                    { value: "Front-end", label: "Front-end" }
                                ]}
                            />
                        </Col>

                        <Col xs={2}>
                            <FormGroup>
                                <InputGroup>
                                    <FormControl
                                        type="text"
                                        onChange={this.handleSearchInputChange}
                                    />
                                    <InputGroup.Button>
                                        <Button onClick={this.handleSearchButtonClick}>
                                            Search
                    </Button>
                                    </InputGroup.Button>
                                </InputGroup>
                            </FormGroup>
                        </Col>
                        <Col xs={1}>
                            <FormGroup>

                               
                                    
                            <ModalComponent onAddNewWorker = {this.addNewWorker}/>
               

                            </FormGroup>
                        </Col>
                    </Row>

                    <Row>
                        
                        <Col xs={12}>
                            <Table bordered condensed hover>
                                <thead>
                                    <tr>

                                        <th>Фамилия</th>
                                        <th>Имя</th>
                                        <th>Отчество</th>
                                        <th>Дата Рождения</th>
                                        <th>Пол</th>
                                        <th>Опыт работы</th>
                                        <th>Опыт работы на текущем месте</th>
                                        <th>Отдел</th>
                                        <th>Должность</th>
                                        <th>Зарплата</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {resultArray.map((item, index) => (
                                        <tr key={index}>
                                            <td>{item.last_name}</td>
                                            <td>{item.name}</td>
                                            <td>{item.patronymic}</td>
                                            <td>{item.dob}</td>
                                            <td>{item.sex}</td>
                                            <td>{item.experience}</td>
                                            <td>{item.experince_atplace}</td>
                                            <td>{item.department}</td>
                                            <td>{item.position}</td>
                                            <td> {item.trainee == true ? item.salary / 2  : item.salary}</td>
                                           <EditModalComponent onEditWorker = {this.EditWorker}  worker={this.state.result[index]} workerindex={index}/> 
                                          <Button className = "RemoveBtn"   onClick={() => this.deleteItem(index) }> <img src="/src/img/remove.png" className="icons"/></Button>       
            
                                        </tr>
                                    ))}
                                </tbody>
                            </Table>
                        </Col>
                       
                    </Row>
                </Grid>

                <footer className="Tip" id="vafel">
                    {/* <img src="/src/img/tip.png" alt="" /> */}
                </footer>
            </div>
        );
    }
}

ReactDOM.render(<Workers />, document.getElementById("root"));

function searchSex(items, sex) {
    return sex === null
        ? items
        // : items.filter(item => item.sex == sex);
        : items.filter(item => item.sex.indexOf(sex) !== -1);
}

function searchDepartment(items, department) {
    return department === null
        ? items
        // : items.filter(item => item.department == department);
        : items.filter(item => item.department.indexOf(department) !== -1);
}

function searchTrainee(items, trainee) {
    return trainee === null
        ? items
        : items.filter(item => item.trainee == trainee);
        // : items.filter(item => item.trainee.indexOf(trainee) !== -1);
}

