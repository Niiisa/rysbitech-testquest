import React from 'react';

import 'semantic-ui-css/semantic.min.css'
import { Modal, Select, Button, Image, Form, Radio, Input, Checkbox, } from 'semantic-ui-react'

export default class EditUserModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            last_name: this.props.worker.last_name,
            name: this.props.worker.name,
            patronymic: this.props.worker.patronymic,
            dob: this.props.worker.dob,
            sex: this.props.worker.sex,
            experience: this.props.worker.experience,
            experince_atplace: this.props.worker.experince_atplace,
            department: this.props.worker.department,
            position: this.props.worker.position,
            salary: this.props.worker.salary,
            trainee: this.props.worker.trainee,

        };


        this.handleEditWorker = this.handleEditWorker.bind(this);

        this.handleNameChange = this.handleNameChange.bind(this);
        this.handlelastNameChange = this.handlelastNameChange.bind(this);
        this.handlePatronymicChange = this.handlePatronymicChange.bind(this);
        this.handleDobChange = this.handleDobChange.bind(this);
        this.handleSexChange = this.handleSexChange.bind(this);
        this.handleExperienceChange = this.handleExperienceChange.bind(this);
        this.handleExperinceAPChange = this.handleExperinceAPChange.bind(this);
        this.handleDepartmentChange = this.handleDepartmentChange.bind(this);
        this.handlePositionChange = this.handlePositionChange.bind(this);
        this.handleSalaryChange = this.handleSalaryChange.bind(this);
        this.handleTraineeChange = this.handleTraineeChange.bind(this);

    }
    handleNameChange(e) {
        this.setState({
            name: e.target.value
        })
    }
    handlelastNameChange(e) {
        this.setState({
            last_name: e.target.value
        })
    }
    handlePatronymicChange(e) {
        this.setState({
            patronymic: e.target.value
        })
    }
    handleDobChange(e) {
        this.setState({
            dob: e.target.value
        })
    }
    handleSexChange(e, { value }) {
        this.setState({
            sex: value
        })
    }

    handleExperienceChange(e) {
        this.setState({
            experience: e.target.value
        })
    }
    handleExperinceAPChange(e) {
        this.setState({
            experince_atplace: e.target.value
        })
    }
    handleDepartmentChange(e, { value }) {
        this.setState({
            department: value
        })
    }
    handlePositionChange(e, { value }) {
        this.setState({
            position: value
        })
    }
    handleSalaryChange(e) {
        this.setState({
            salary: e.target.value
        })
    }
    handleTraineeChange(e) {
        this.setState((prevState) => ({ trainee: !prevState.trainee }))
    }


    handleEditWorker() {
        this.props.onEditWorker(this.props.workerindex, {
            last_name: this.state.last_name,
            name: this.state.name,
            patronymic: this.state.patronymic,
            dob: this.state.dob,
            sex: this.state.sex,
            experience: this.state.experience,
            experince_atplace: this.state.experince_atplace,
            department: this.state.department,
            position: this.state.position,
            salary: this.state.salary,
            trainee: this.state.trainee,
        })
    }



    render() {
        return (
            <Modal trigger={<Button ui icon basic button borderless>
                <Image src="/src/img/edit.png" className="icons" />
            </Button>}>
                <Modal.Header>Изменить информацию о сотрудниксе</Modal.Header>
                <Modal.Content>
                    <Form>

                        <Form.Field
                            control={Input}
                            label='Фамилия'
                            placeholder='Last name'
                            onChange={this.handlelastNameChange}
                            value={this.state.last_name}
                        />
                        <Form.Field
                            control={Input}
                            label='Имя'
                            placeholder='First name'
                            onChange={this.handleNameChange}
                            value={this.state.name}
                        />
                        <Form.Field
                            control={Input}
                            label='Отчество'
                            placeholder='patronymic'
                            onChange={this.handlePatronymicChange}
                            value={this.state.patronymic}
                        />
                        <Form.Field
                            control={Input}
                            label='Дата Рождения'
                            placeholder='date'
                            onChange={this.handleDobChange}
                            value={this.state.dob}
                        />
                        <Form.Field label='Пол' />
                        <Form.Field>

                            <Radio
                                label='M'
                                name='radioGroup'
                                value='мужчина'
                                onChange={this.handleSexChange}
                                checked={this.state.sex === 'мужчина'}
                            />
                        </Form.Field>
                        <Form.Field>
                            <Radio
                                label='Ж'
                                name='radioGroup'
                                value='женщина'
                                onChange={this.handleSexChange}
                                checked={this.state.sex === 'женщина'}

                            />
                        </Form.Field>
                        <Form.Field
                            control={Input}
                            label='Опыт работы'
                            placeholder='exp'
                            onChange={this.handleExperienceChange}
                            value={this.state.experience}

                        />
                        <Form.Field
                            control={Input}
                            label='Опыт работы на текущей должности'
                            placeholder='exp at p'
                            onChange={this.handleExperinceAPChange}
                            value={this.state.experince_atplace}
                        />
                        <Form.Field
                            control={Select}
                            label='Отдел'
                            placeholder='department'

                            options={[{ value: "Testing", text: "Testing" },
                            { value: "Back-end", text: "Back-end" },
                            { value: "Analytics", text: "Analytics" },
                            { value: "Front-end", text: "Front-end" }]}
                            onChange={this.handleDepartmentChange}
                            value={this.state.department}
                        />
                        <Form.Field
                            control={Select}
                            label='Должность'
                            placeholder='position'

                            options={[

                                { value: "Инженер", text: "Инженер" },
                                { value: "Старший Инженер", text: "Старший Инженер" },
                                { value: "Тестировщик", text: "Тестировщик" },
                                { value: "Аналитик", text: "Аналитик" },
                            ]}
                            onChange={this.handlePositionChange}
                            value={this.state.position}
                        />
                        <Form.Field
                            control={Input}
                            label='Зарплата'
                            placeholder='salary'
                            onChange={this.handleSalaryChange}
                            value={this.state.salary}
                        />
                        <Form.Field
                            control={Checkbox}
                            label='Стажёр'
                            placeholder='trainee'
                            onChange={this.handleTraineeChange}
                            checked={this.state.trainee}
                        />


                        <Form.Field control={Button}
                            onClick={this.handleEditWorker}
                        >Edit</Form.Field>
                    </Form>
                </Modal.Content>
            </Modal>
        )
    }
}